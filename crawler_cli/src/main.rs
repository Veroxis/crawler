use crawler_cli::run;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    run()?;
    Ok(())
}
