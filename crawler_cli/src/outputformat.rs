use std::collections::HashMap;

use crawler_lib::response::ResponseState;

#[derive(Debug, Clone)]
pub enum OutputFormat {
    Json,
    Debug,
    Ron,
    BinCode,
    Cbor,
    Pickle,
    Yaml,
    MessagePack,
}

impl OutputFormat {
    const FORMAT_STRINGS: &'static [&'static str] = &[
        "json",
        "debug",
        "ron",
        "bincode",
        "cbor",
        "pickle",
        "yaml",
        "messagepack",
    ];

    pub fn serialize_response_data(
        &self,
        response_data: &HashMap<String, ResponseState>,
    ) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        match self {
            OutputFormat::Json => Ok(serde_json::to_string(&response_data)?.as_bytes().to_vec()),
            OutputFormat::Debug => Ok(format!("{:#?}", response_data).as_bytes().to_vec()),
            OutputFormat::Ron => Ok(ron::ser::to_string(&response_data)?.as_bytes().to_vec()),
            OutputFormat::BinCode => Ok(bincode::serialize(&response_data)?),
            OutputFormat::Cbor => Ok(serde_cbor::ser::to_vec(&response_data)?),
            OutputFormat::Pickle => Ok(serde_pickle::to_vec(&response_data, true)?),
            OutputFormat::Yaml => Ok(serde_yaml::to_string(&response_data)?.as_bytes().to_vec()),
            OutputFormat::MessagePack => Ok(rmp_serde::to_vec(&response_data)?),
        }
    }

    pub fn get_formats() -> &'static [&'static str] {
        OutputFormat::FORMAT_STRINGS
    }

    pub fn from_str(input: &str) -> Result<Self, Box<dyn std::error::Error>> {
        match input {
            "json" => Ok(OutputFormat::Json),
            "debug" => Ok(OutputFormat::Debug),
            "ron" => Ok(OutputFormat::Ron),
            "bincode" => Ok(OutputFormat::BinCode),
            "cbor" => Ok(OutputFormat::Cbor),
            "pickle" => Ok(OutputFormat::Pickle),
            "yaml" => Ok(OutputFormat::Yaml),
            "messagepack" => Ok(OutputFormat::MessagePack),
            unknown => Err(Box::new(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Unknown OutputFormat: {}", unknown),
            ))),
        }
    }
}
