pub struct Ui {
    terminal: terminal::Terminal<std::io::Stdout>,
    progress: bool,
}

impl Ui {
    pub fn new(progress: bool) -> Self {
        Ui {
            terminal: terminal::stdout(),
            progress,
        }
    }

    pub fn render(&mut self, content: &str) -> Result<(), Box<dyn std::error::Error>> {
        if !self.progress {
            return Ok(());
        }
        self.terminal
            .act(terminal::Action::ClearTerminal(terminal::Clear::All))?;
        self.terminal.act(terminal::Action::MoveCursorTo(0, 0))?;
        std::io::Write::write_all(&mut self.terminal, format!("{}\n", &content).as_bytes())?;
        std::io::Write::flush(&mut self.terminal)?;
        Ok(())
    }

    pub fn sleep(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if !self.progress {
            return Ok(());
        }
        std::io::Write::write(
            &mut self.terminal,
            b"<== Press ENTER to continue program ==>",
        )?;
        std::io::Write::flush(&mut self.terminal)?;
        self.terminal.get(terminal::Value::Event(None))?;
        Ok(())
    }
}
