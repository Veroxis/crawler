extern crate crawler_lib;
mod cliconfig;
mod outputformat;
mod ui;
use crate::crawler::AmountThreads;
use cliconfig::CliConfig;
use core::time;
use crawler_lib::{
    crawler::{self, Crawler, CrawlerState},
    response::ResponseState,
};
use std::{
    fs::File,
    io::{prelude::*, BufRead, BufReader},
    path::Path,
    thread,
};
use ui::Ui;

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let conf = CliConfig::new()?;
    let urls = load_urls_from_file(&conf.input_file_path)?;
    let mut crawler = Crawler::new(urls, AmountThreads(conf.threads));
    let mut state;
    let mut ui = match conf.progress {
        true => Some(Ui::new(conf.progress)),
        false => None,
    };
    loop {
        state = crawler.poll();
        match state {
            CrawlerState::Created => crawler.run(),
            CrawlerState::Running => {
                if let Some(mut local_ui) = ui.as_mut() {
                    print_status(&mut crawler, &conf, &mut local_ui)?;
                }
                thread::sleep(time::Duration::from_millis(100));
            }
            CrawlerState::Finished => {
                if let Some(mut local_ui) = ui.as_mut() {
                    print_status(&mut crawler, &conf, &mut local_ui)?;
                }
                break;
            }
        }
    }
    let response_data = crawler.get_urls();
    let output = conf.format.serialize_response_data(&response_data)?;
    write_bytes_to_file(&conf.output_file_path, &output)?;
    if let Some(mut local_ui) = ui.as_mut() {
        print_status(&mut crawler, &conf, &mut local_ui)?;
        local_ui.sleep()?;
    }
    Ok(())
}

fn write_bytes_to_file(
    output_file_path: &Path,
    bytes: &[u8],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut pos = 0;
    let mut buffer = File::create(output_file_path)?;
    while pos < bytes.len() {
        let bytes_written = buffer.write(&bytes[pos..])?;
        pos += bytes_written;
    }
    Ok(())
}

fn print_status(
    crawler: &mut Crawler,
    conf: &CliConfig,
    ui: &mut Ui,
) -> Result<(), Box<dyn std::error::Error>> {
    let urls = crawler.get_urls();
    let total_counter = urls.len();
    let mut pending_counter = 0;
    let mut in_progress_counter = 0;
    let mut finished_counter = 0;
    for (_url, state) in urls.iter() {
        match state {
            ResponseState::Pending => pending_counter += 1,
            ResponseState::InProgress { thread_id: _ } => in_progress_counter += 1,
            ResponseState::Response { response: _ } => finished_counter += 1,
        }
    }
    let content = format!(
        "{separator}
[Config]
Input File: {input_file:?}
Output File: {output_file:?}
Output Format: {output_format:?}
Threads: {threads}
{separator}
[Urls]
Total: {total}
Pending: {pending}
In Progress: {in_progress}
Finished: {finished}
{separator}",
        separator =
            "================================================================================",
        total = total_counter,
        pending = pending_counter,
        in_progress = in_progress_counter,
        finished = finished_counter,
        threads = &conf.threads,
        input_file = &conf.input_file_path,
        output_file = &conf.output_file_path,
        output_format = &conf.format
    );
    ui.render(&content)?;
    Ok(())
}

fn load_urls_from_file(filename: &Path) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let file = File::open(filename)?;
    let buf = BufReader::new(file);
    let urls = buf
        .lines()
        .map(|l| l.unwrap_or_else(|_| String::from("")).trim().to_string())
        .collect();
    Ok(urls)
}
