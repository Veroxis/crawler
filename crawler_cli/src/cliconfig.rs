use clap::{App, Arg};
use std::path::PathBuf;

use crate::outputformat::OutputFormat;

#[derive(Debug, Clone)]
pub struct CliConfig {
    pub input_file_path: PathBuf,
    pub output_file_path: PathBuf,
    pub threads: u32,
    pub format: OutputFormat,
    pub progress: bool,
}

impl CliConfig {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
        let cpus = num_cpus::get().to_string();
        let matches = App::new("Crawler")
            .version("0.1.0")
            .author("Daniel Kupczak <d.kupczak94@gmail.com>")
            .about("Yet another crawler")
            .arg(
                Arg::with_name("input-file")
                    .short("i")
                    .long("input-file")
                    .takes_value(true)
                    .required(true)
                    .help("path to file containing urls"),
            )
            .arg(
                Arg::with_name("output-file")
                    .short("o")
                    .long("output-file")
                    .takes_value(true)
                    .required(true)
                    .help("where to write the responsedata"),
            )
            .arg(
                Arg::with_name("threads")
                    .short("t")
                    .long("threads")
                    .takes_value(true)
                    .default_value(cpus.as_str())
                    .help("Amount of threads to use"),
            )
            .arg(
                Arg::with_name("format")
                    .short("f")
                    .long("format")
                    .takes_value(true)
                    .default_value(OutputFormat::get_formats()[0])
                    .possible_values(OutputFormat::get_formats())
                    .help("which format to write output in"),
            )
            .arg(
                Arg::with_name("progress")
                    .short("p")
                    .long("progress")
                    .takes_value(false)
                    .help("show crawler progress"),
            )
            .get_matches();

        let input_file = matches
            .value_of("input-file")
            .ok_or("Empty param 'input-file'")?;
        let mut input_file_path = PathBuf::new();
        input_file_path.push(input_file);
        if !input_file_path.exists() {
            eprintln!("File doesn't exist: {}", input_file);
            std::process::exit(1);
        }
        let output_file = matches
            .value_of("output-file")
            .ok_or("Empty param 'output-file'")?;
        let mut output_file_path = PathBuf::new();
        output_file_path.push(output_file);
        let threads = matches.value_of("threads").ok_or("Empty param 'threads'")?;
        let threads = match threads.parse::<u32>() {
            Ok(num) => num,
            Err(e) => {
                eprintln!(
                    "The parameter for 'threads' should be the number of threads to use (u32): {:?}",
                    e
                );
                std::process::exit(1);
            }
        };
        let format =
            OutputFormat::from_str(matches.value_of("format").ok_or("Empty param 'format'")?)?;
        let progress = matches.is_present("progress");
        Ok(CliConfig {
            input_file_path,
            output_file_path,
            threads,
            format,
            progress,
        })
    }
}
