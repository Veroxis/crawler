use crate::{response::ResponseState, worker::Worker};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

pub type UrlMap = Arc<Mutex<HashMap<String, ResponseState>>>;

#[derive(Debug)]
pub struct Crawler {
    state: CrawlerState,
    workers: Vec<Worker>,
    amount_urls: u32,
    urls: UrlMap,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum CrawlerState {
    Created,
    Running,
    Finished,
}
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct AmountThreads(pub u32);

impl Crawler {
    pub fn new(urls: Vec<String>, amount_threads: AmountThreads) -> Self {
        let mut crawler = Crawler {
            workers: Vec::new(),
            state: CrawlerState::Created,
            amount_urls: 0,
            urls: Arc::new(Mutex::new(HashMap::<String, ResponseState>::new())),
        };
        for url in urls {
            crawler
                .urls
                .lock()
                .unwrap()
                .insert(url, ResponseState::Pending);
        }
        crawler.amount_urls = crawler.urls.lock().unwrap().len() as u32;
        for id in 1..amount_threads.0 + 1 {
            crawler.workers.push(Worker::new(id, crawler.urls.clone()));
        }
        crawler
    }

    pub fn run(&mut self) {
        match self.state {
            CrawlerState::Created => {
                for worker in self.workers.iter_mut() {
                    worker.run();
                }
                self.state = CrawlerState::Running;
            }
            CrawlerState::Running => {
                eprintln!("Warning: The crawler is already running.");
            }
            CrawlerState::Finished => {
                eprintln!("Warning: The crawler has already finished.");
            }
        }
    }

    pub fn poll(&mut self) -> CrawlerState {
        match self.state {
            CrawlerState::Created => {}
            CrawlerState::Running => {
                let mut finished = true;
                for (_, response_state) in self.urls.lock().unwrap().iter_mut() {
                    match response_state {
                        ResponseState::Pending | ResponseState::InProgress { thread_id: _ } => {
                            finished = false;
                            break;
                        }
                        ResponseState::Response { response: _ } => continue,
                    }
                }
                if finished {
                    self.state = CrawlerState::Finished;
                    for worker in self.workers.iter_mut() {
                        worker.request_stop();
                    }
                    for worker in self.workers.iter_mut() {
                        worker.join();
                    }
                }
            }
            CrawlerState::Finished => {}
        }
        self.state.clone()
    }

    pub fn get_crawler_status(&mut self) -> String {
        let mut buffer = Vec::new();
        for worker in self.workers.iter_mut() {
            buffer.push(worker.get_status());
        }
        buffer.join("\n  ")
    }

    pub fn get_urls(&mut self) -> HashMap<String, ResponseState> {
        self.urls.lock().unwrap().clone()
    }
}
