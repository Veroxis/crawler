use curl::easy::Easy;
use serde::{Deserialize, Serialize};
use std::time::Duration;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Response {
    pub url: String,
    pub response_code: Result<u32, String>,
    pub total_time: Result<Duration, String>,
    pub namelookup_time: Result<Duration, String>,
    pub connect_time: Result<Duration, String>,
    pub appconnect_time: Result<Duration, String>,
    pub pretransfer_time: Result<Duration, String>,
    pub starttransfer_time: Result<Duration, String>,
    pub redirect_time: Result<Duration, String>,
}

impl Response {
    pub fn new(url: &str) -> Option<Self> {
        let mut handle = Easy::new();
        match handle.url(url) {
            Ok(_) => {
                if handle.perform().is_err() {
                    return None;
                }
                Some(Response {
                    url: String::from(url),
                    response_code: reformat_curl_result(handle.response_code()),
                    total_time: reformat_curl_result(handle.total_time()),
                    namelookup_time: reformat_curl_result(handle.namelookup_time()),
                    connect_time: reformat_curl_result(handle.connect_time()),
                    appconnect_time: reformat_curl_result(handle.appconnect_time()),
                    pretransfer_time: reformat_curl_result(handle.pretransfer_time()),
                    starttransfer_time: reformat_curl_result(handle.starttransfer_time()),
                    redirect_time: reformat_curl_result(handle.redirect_time()),
                })
            }
            Err(_) => None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum ResponseState {
    Pending,
    InProgress { thread_id: u32 },
    Response { response: Box<Option<Response>> },
}

fn reformat_curl_result<T>(curl_result: Result<T, curl::Error>) -> Result<T, String> {
    match curl_result {
        Ok(value) => Ok(value),
        Err(err) => Err(err.to_string()),
    }
}
