use crate::{
    crawler::UrlMap,
    response::{Response, ResponseState},
};
use serde::{Deserialize, Serialize};
use std::{
    sync::{Arc, Mutex},
    thread, time,
};

#[derive(Debug)]
pub struct Worker {
    id: u32,
    current_url: Arc<Mutex<Option<String>>>,
    state: Arc<Mutex<WorkerState>>,
    urls: UrlMap,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum WorkerState {
    Created,
    Running,
    Stopping,
    Stopped,
}

impl Worker {
    pub fn new(id: u32, urls: UrlMap) -> Self {
        Worker {
            id,
            current_url: Arc::new(Mutex::new(None)),
            state: Arc::new(Mutex::new(WorkerState::Created)),
            urls,
        }
    }

    pub fn run(&mut self) {
        let id = self.id;
        let current_url = self.current_url.clone();
        let state = self.state.clone();
        let urls = self.urls.clone();
        thread::spawn(move || {
            loop {
                let current_state = *state.lock().expect("Failed to lock Mutex");
                match current_state {
                    WorkerState::Created => {
                        *state.lock().expect("Failed to lock Mutex") = WorkerState::Running;
                    },
                    WorkerState::Running => {
                        let mut local_current_url = None;
                        for (url, response_state) in urls.lock().expect("Failed to lock Mutex").iter_mut() {
                            match response_state {
                                ResponseState::Pending => {
                                    local_current_url = Some(url.clone());
                                    *current_url.lock().expect("Failed to lock Mutex") = local_current_url.clone();
                                    *response_state = ResponseState::InProgress { thread_id: id };
                                    break;
                                },
                                ResponseState::InProgress { thread_id: _ } => continue,
                                ResponseState::Response { response: _ } => continue,
                            }
                        }
                        match local_current_url {
                            Some(url) => {
                                let response = Response::new(url.as_str());
                                urls.lock().expect("Failed to lock Mutex").insert(url, ResponseState::Response { response: Box::new(response) });
                            },
                            None => thread::sleep(time::Duration::from_millis(1))
                        }
                    },
                    WorkerState::Stopping => {
                        break;
                    },
                    WorkerState::Stopped => eprintln!("ERROR: Thread {}: Matched the state 'Stopped'. This code shouldn't be reachable.", id)
                }
            }
            *state.lock().expect("Failed to lock Mutex") = WorkerState::Stopped;
        });
    }

    pub fn get_status(&mut self) -> String {
        let current_state = *self.state.lock().expect("Failed to lock Mutex");
        match current_state {
            WorkerState::Created => format!("Worker Thread: {}, State: Created", &self.id),
            WorkerState::Running => format!(
                "Worker Thread: {}, State: Running, Url: [{}]",
                &self.id,
                self.current_url
                    .lock()
                    .unwrap()
                    .clone()
                    .get_or_insert("None".to_string())
            ),
            WorkerState::Stopping => format!("Worker Thread: {}, State: Stopping", &self.id),
            WorkerState::Stopped => format!("Worker Thread: {}, State: Stopped", &self.id),
        }
    }

    pub fn request_stop(&mut self) {
        *self.state.lock().expect("Failed to lock Mutex") = WorkerState::Stopping;
    }

    pub fn join(&mut self) {
        loop {
            let state = *self.state.lock().expect("Failed to lock Mutex");
            match state {
                WorkerState::Created => break,
                WorkerState::Running => self.request_stop(),
                WorkerState::Stopping => thread::sleep(time::Duration::from_millis(1)),
                WorkerState::Stopped => break,
            }
        }
    }
}
